#include <stdio.h>
//#include <R.h>
#include <string.h>

void crunBMLGridOld(int *grid, int *numSteps, int *row, int *col, int *nextgrid, int *movedCars, int *blockedCars, int *nblue, int *nred){
    
    int i, k;
    // int n = 0;
    int total = (*row) * (*col);
    int red[total], blue[total];
    
    
    for ( i = 0; i < total; i++) {
        if (grid[i] == 2) red[i] = 2;
        else red[i] = 1;// 2 stands for existing red cars
    }
    for ( i = 0; i < total; i++) {
        if (grid[i] == 3) blue[i] = 3;
        else blue[i] = 1;// 3 stands for existing blue cars
    }
    
    
    
    
    for ( k = 0; k < *numSteps; k++) {
        int n = 0;//save the movedcars number
        
        //move blue cars
        if (k % 2 == 0){
            for ( i = 0; i < total; i++) {
                if (grid[i] == 3) {
                    if (((i + 1) % (*row) == 0) && (grid[i - *row + 1] == 1)) {
                        blue[ i - *row + 1] = 3;
                        blue[ i ] = 1;
                        n = n + 1;
                    }
                    
                    if (((i + 1) % (*row) != 0) && (grid[i + 1] == 1)){
                        blue[i + 1] = 3;
                        blue[ i ] = 1;
                        n = n + 1;
                    }
                    
                }
                
            }
            
            
            for ( i = 0; i < total; i++) {
                if ((blue[i] == 1) && (red[i] == 1)) nextgrid[i] = 1;
                else {
                    if (red[i] == 2) nextgrid[i] = 2;
                    else nextgrid[i] = 3;
                }
            }
            memcpy( grid, nextgrid, sizeof( int )* (*row) * (*col) );
            blockedCars[k] = *nblue - n;
            
        }
        
        //move red cars
        else{
            for ( i = 0; i < total; i++) {
                if (grid[i] == 2) {
                    if ( (i >= (*row) * (*col - 1)) && (grid[i - (*row) * (*col - 1)] == 1)) {
                        red[ i - (*row) * (*col - 1) ] = 2;
                        red[ i ] = 1;
                        n = n + 1;
                    }
                    if ((i < (*row) * (*col - 1)) && (grid[i + (*row)] == 1)){
                        red[i + (*row)] = 2;
                        red[ i ] = 1;
                        n = n + 1;
                    }
                }
            }
            
            for ( i = 0; i < total; i++) {
                if ((blue[i] == 1) && (red[i] == 1)) nextgrid[i] = 1;
                else if (red[i] == 2) nextgrid[i] = 2;
                else nextgrid[i] = 3;
            }
            
            memcpy( grid, nextgrid, sizeof( int )* (*row) * (*col) );
            blockedCars[k] = *nred - n;
            
        }
        movedCars[k] = n;
        
    }
    
}
